import express from 'express';
import database from "./models";
import { Request, Response } from 'express';
import cors from 'cors';
import { UserRoutes } from './routes/user';
import { AuthRoutes } from './routes/auth';
import {MeetingRoutes} from "./routes/meetings";
import {ProjectRoutes} from "./routes/project";
import {AdminRoutes} from "./routes/admin";
import {errorHandler} from "./middlewares/error.handler";
import {NotificationRoutes} from "./routes/notification";
import {TaskRoutes} from "./routes/task";

const app = express();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());

app.listen(4040, () => console.log('Listening on port: 4040'));

app.use('/', AuthRoutes);
app.use('/user', UserRoutes);
app.use('/meeting', MeetingRoutes);
app.use('/project', ProjectRoutes);
app.use('/admin', AdminRoutes);
app.use('/notification', NotificationRoutes);
app.use('/task', TaskRoutes);

/* it's important to be after all routes */
app.use(errorHandler);

(async () => {
   try {
      await database.sequelize.sync({ alter: true });
   } catch (error) {
      console.error(error);
   }
})();

database.sequelize.authenticate()
   .then(() => console.log('authenticated'))
   .catch((error: any) => console.log(error));