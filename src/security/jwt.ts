import * as jwt from 'jsonwebtoken';
import { v4 as uuidv4 } from 'uuid';
import {JwtPayload} from "jsonwebtoken";

class PayloadJWT implements JwtPayload {
    id: number;
    username: string;
    role: number;
    picture: string;
}

export class JWT {
    private static JWT_SECRETE = 'jwt-secrete';

    /**
     * Generate JWT token with specified payload
     * @param user any
     * @return string
     * **/
    public static async generateToken(user: any) {
        const payload = {
            id: user.id,
            username: user.username,
            role: user.role,
            picture: user.picture
        }

        return jwt.sign(payload, this.JWT_SECRETE, {
            expiresIn: '30d',
            jwtid: uuidv4(),
        });
    }

    public static isTokenValid(token: string) {
        return jwt.verify(token, this.JWT_SECRETE, {
           ignoreExpiration: false
        });
    }

    public static decode(token: string): PayloadJWT {
        return <PayloadJWT>jwt.decode(token);
    }
}