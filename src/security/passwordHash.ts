import * as bcrypt from 'bcrypt';

export class PasswordHash {

    /** Generate unique hash, salt is also added in case there are collisions
     * @param plainPassword string
     * @return Promise<string>
     * **/
    public static async hashPassword(plainPassword: string): Promise<string> {
        const salt = await bcrypt.genSalt(10);
        return await bcrypt.hash(plainPassword, salt);
    }

    /**
     * Check if the supplied password is equal to the stored one
     * @param plainPassword string
     * @param hashedPassword string
     * **/
    public static async isPasswordValid(plainPassword: string, hashedPassword: string) {
        return await bcrypt.compare(plainPassword, hashedPassword);
    }

}