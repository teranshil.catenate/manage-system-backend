import { Request, Response } from "express";
import {JWT} from "../security/jwt";
import {ErrorDTO} from "../dto/response/error.dto";
import {ROLES} from "../constants";

export const authorization = (request: Request, response: Response, next: any) => {
    try {
        const token: string = request.headers['authorization']?.split(' ')[1] || '';
        console.log(token);
        if(token === '') throw new Error('JWT is not supplied!');

        JWT.isTokenValid(token);
        request.body.token = token;
        request.body.authenticated = JWT.decode(request.body.token);
    } catch (error) {
        if(error instanceof Error) {
            const errorDTO: ErrorDTO = new ErrorDTO();
            errorDTO.status = 401;
            errorDTO.message = error.message;

            return response.status(401).json(errorDTO);
        }
    }

    next();
};

export const permission = (request: Request, response: Response, next: any) => {
    try {
       const decoded = JWT.decode(request.body.token);

       if(decoded.role !== ROLES.ADMIN) throw new Error("Don't have a permission!");
    } catch (error) {
        if(error instanceof Error) {
            const errorDTO: ErrorDTO = new ErrorDTO();
            errorDTO.status = 401;
            errorDTO.message = error.message;

            return response.status(401).json(errorDTO);
        }
    }

    next();
}


