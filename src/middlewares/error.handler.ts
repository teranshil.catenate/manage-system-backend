import {Request, Response} from "express";
import {Error, ValidationError} from "sequelize";
import {ErrorDTO} from "../dto/response/error.dto";
import {InvalidDataError} from "../errors/InvalidDataError";

export const errorHandler = (error: Error, request: Request, response: Response, next: any) => {
    const failure = new ErrorDTO();

    if(error instanceof ValidationError) {
        failure.message = error.errors.map(record => record.message).join('\n');
        failure.status = 422;
        response.status(failure.status).json(failure);
    } else if(error instanceof InvalidDataError) {
        failure.message = error.params.message;
        failure.status = error.params.status;
    } else {
        failure.message = error.message;
        failure.status = 500;
    }

    response.status(failure.status).json(failure);
};