import multer from "multer";

const storage = multer.diskStorage({
    destination: (request, file, callback) => {
        console.log(file, 'heere');
        callback(null, './public');
    },
    filename(request, file, callback) {
        console.log(file, 'heere');
        callback(null,  Date.now() + '-' + file.originalname );
    }
});

export const upload = multer({ storage });