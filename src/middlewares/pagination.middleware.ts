import {Request, Response} from "express";
import {QUERY_ORDER} from "../constants";

export const pagination = (request: Request, response: Response, next: any) => {
    const offset = request.query.offset ?? 0;
    const limit = request.query.count ?? 100;
    const field = request.query.field;
    const ordering = request.query.order;
    const order: string[][] = [];

    if(field && ordering && typeof field === 'string' && typeof ordering === 'string') {
        order.push( [field, QUERY_ORDER[parseInt(ordering)]] );
    }
    request.body.pagination = { offset, limit, order };

    next();
};