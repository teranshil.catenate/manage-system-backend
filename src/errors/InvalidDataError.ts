import {Error} from "sequelize";
import { BasicErrorType } from "./basic.error.type";

export class InvalidDataError extends Error {

    constructor(public params: BasicErrorType) {
        super(params.message);
    }
}