export class BasicErrorType {
    message: string;
    status: number;
}