export class PaginationType {
    offset?: number;
    limit?: number;
    order?: string[][]
}