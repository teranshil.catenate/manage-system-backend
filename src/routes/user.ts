import * as express from 'express';
import {Response, Request, NextFunction} from 'express';
import database from "../models/index";
import {Model, Op} from "sequelize";
import {authorization, permission} from "../middlewares/authentication.middleware";
import {UserUpdateDto} from "../dto/request/user-update.dto";
import {PasswordHash} from "../security/passwordHash";
import {ErrorDTO} from "../dto/response/error.dto";
import {AuthenticationDTO} from "../dto/response/authentication.dto";
import {UserDTO} from "../dto/response/user.dto";
import {JWT} from "../security/jwt";
import { upload } from "../middlewares/upload.public.middleware";
import {SuccessResponseDTO} from "../dto/response/success.response.dto";
import {SuccessDTO} from "../dto/response/success.dto";
import {pagination} from "../middlewares/pagination.middleware";
import {InvalidDataError} from "../errors/InvalidDataError";
import {STATUSES} from "../constants";
import UserService from "../services/UserService";

const UserRouter = express.Router();
const userService: UserService = new UserService();


/**
 * Gets all users with pagination, if it's not supplied by request default pagination will take place.
 * Admin only have access
 * **/
UserRouter.get('/', authorization, permission, pagination, async (request: Request, response: Response, next: NextFunction) => {
    try {
       const result = await userService.getUsers(request, response, next);

       const success = new SuccessDTO();
       success.message = 'Successfully retrieved users!';
       success.status = 200;
       success.data = result.rows;
       success.pagination = {
          total: result.count,
          offset: request.body.pagination.offset,
          count: request.body.pagination.limit
        }

       response.status(success.status).json(success);
    } catch (error) {
        next(error);
    }
});

UserRouter.put('/', authorization, upload.single('picture'), async (request: Request, response: Response, next: NextFunction) => {
    try {
        const updatedUser = await userService.updateUser(request, response, next);

        const authenticationDTO: AuthenticationDTO = new AuthenticationDTO();
        authenticationDTO.user = new UserDTO(updatedUser);
        authenticationDTO.token = await JWT.generateToken(updatedUser);

        response.status(200).json(authenticationDTO);
    } catch (error) {
        next(error);
    }
});

UserRouter.delete('/:id', authorization, permission, async (request: Request, response: Response, next: NextFunction) => {

    try {
        await userService.deleteUserById(request);

        const success: SuccessDTO = new SuccessDTO();
        success.status = 200;
        success.message = 'User successfully deleted with its associations!';
        success.data = null;

        response.status(success.status).json(success);
    } catch (error) {
        next(error);
    }
});

UserRouter.post('/:userId/project/:projectId', authorization, async (request: Request, response: Response, next: NextFunction) => {
    try {
        await userService.assignUserToProject(request);

        const success: SuccessDTO = new SuccessDTO();
        success.status = 200;
        success.message = 'User assigned to project';
        success.data = null;

        response.status(success.status).json(success);
    } catch (error) {
        next(error);
    }
});

UserRouter.delete('/:userId/project/:projectId', authorization, async (request: Request, response: Response, next: NextFunction) => {
    try {
        await userService.removeUserFromProject(request);

        const success: SuccessDTO = new SuccessDTO();
        success.status = 200;
        success.message = 'User unassigned from project';
        success.data = null;

        response.status(success.status).json(success);
    } catch (error) {
        next(error);
    }
});


UserRouter.get('/meetings', authorization, async (request: Request, response: Response, next: NextFunction) => {

    try {
        const meetings = userService.getUserMeetingsDetailed(request);
        // const userId = request.body.authenticated.id;
        // const meetings = await database.Meeting.findAll({ include: [ { model: database.User, as: 'users', attributes: [],
        //                                                                       where: { id: userId }, through: { attributes: [] }},
        //                                                                     { model: database.Project, as: 'project', attributes: ['id', 'name'] } ]});
        const success: SuccessDTO = new SuccessDTO();
        success.status = 200;
        success.message = 'Successfully retrieved meetings!';
        success.data = meetings;

        response.status(success.status).json(success);
    } catch (error) {
        next(error);
    }
});

UserRouter.get('/:id/meetings', authorization, permission, async (request: Request, response: Response, next: NextFunction) => {
    try {
     const userId: number =  Number(request.params.id);

     // const test = await database.UserMeetings.findAll({ where: { id: userId } })

     const meetings = await database.Meeting.findAll({ include: { model: database.User, as: 'users',
                                                     attributes: [ 'id', 'name' ], where: { id: userId }} });

     for (const meeting of meetings) {
         const allUserAssigned = await database.UserMeetings.findAll({ attributes: ['user_id'], where: { 'meeting_id': meeting.id } });

         meeting.setDataValue('users_id', allUserAssigned.map((userMeeting: any ) => userMeeting.user_id));
         console.log(meeting);
         console.log(allUserAssigned.map((userMeeting: any ) => userMeeting.user_id));
     }

     const success: SuccessDTO = new SuccessDTO();
     success.status = 200;
     success.message = "All user's meetings successfully retrieved!";
     success.data = meetings;

     response.status(success.status).json(success);
    } catch (error) {
        next(error);
    }
});

UserRouter.get('/:id/projects', authorization, permission, (request: Request, response: Response, next: NextFunction) => {
    database.Project.findAll({ include: [ { model: database.User, as: 'users', where: { 'id': request.params.id }, attributes: [] }]})
        .then((projects: Model[]) => {
            const success: SuccessResponseDTO = new SuccessResponseDTO();
            success.status = 200;
            success.message = "Successfully retrieved user's projects";
            success.data = projects;

            response.status(success.status).json(success);
        })
        .catch((error: Error) => {
            next(error);
        });
})

UserRouter.get('/projects', authorization, async (request: Request, response: Response, next: NextFunction) => {
    const decode = JWT.decode(request.body.token);

    database.Project.findAll({
        attributes: ['id', 'name', 'budget', 'status', 'description', 'start_date', 'finish_date'],
        include: [ {
            model: database.User,
            as: 'users',
            where: { 'id': decode.id },
            attributes: [ 'picture', 'name' ],
            through: { attributes: [] }
        }]
    }).then((projects: Model[]) => {
            const success: SuccessResponseDTO = new SuccessResponseDTO();
            success.status = 200;
            success.message = "Successfully retrieved user's projects";
            success.data = [ ...projects ];

            response.status(200).json(success);
        })
        .catch((error: Error) => next(error));
});

UserRouter.get('/tasks', authorization, async (request: Request, response: Response, next: NextFunction) => {

    try {
        const userId = request.body.authenticated.id;
        const tasks = await database.Task.findAll({ attributes: [ 'id', 'description', 'title', 'start_date', 'end_date' ], where: { 'user_id': userId } });

        const success: SuccessDTO = new SuccessDTO();
        success.status = 200;
        success.message = 'Successfully retrieved tasks!';
        success.data = tasks;

        response.status(success.status).json(success);
    } catch (error) {
        next(error);
    }
});


export { UserRouter as UserRoutes }
