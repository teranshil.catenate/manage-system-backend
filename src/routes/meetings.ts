import * as express from "express";
import {Response, Request, NextFunction} from "express";
import {authorization, permission} from "../middlewares/authentication.middleware";
import {MeetingCreateDto} from "../dto/request/meeting-create.dto";
import database from "../models";
import {Error, Model, ValidationError, where} from "sequelize";
import {MeetingUpdateDto} from "../dto/request/meeting-update.dto";
import {SuccessDTO} from "../dto/response/success.dto";
import {InvalidDataError} from "../errors/InvalidDataError";
import {pagination} from "../middlewares/pagination.middleware";
import {STATUSES} from "../constants";

const MeetingsRouter = express.Router();

MeetingsRouter.use(authorization);

MeetingsRouter.get('/', permission, pagination, async (request: Request, response: Response, next: NextFunction) => {

    const { offset, limit, order } = request.body.pagination;

    database.User.findAndCountAll({ attributes: [ 'id', 'name', 'username' ],
        include: { model: database.Meeting, as: 'meetings', through: { attributes: [] } }, offset, limit, order })
        .then((result: { count: number[], rows: Model[] }) => {
            const success = new SuccessDTO();

            success.message = 'Successfully retrieved meetings';
            success.status = 200;
            success.data = result.rows;
            success.pagination = {
                total: result.count,
                offset: offset,
                count: limit
            };

            response.status(200).json(success);
        })
        .catch((error: Error) => {
            next(error);
        });
});

MeetingsRouter.post('/', permission, async (request: Request, response: Response, next: NextFunction) => {
    const body: MeetingCreateDto  = request.body;

    try {
        const project = await database.Project.findOne({ where: { id: body.project_id } });
        if(project === null) throw new InvalidDataError({ message: 'Invalid project ID', status: 422 });

        const meeting = await database.Meeting.create({ ...body });
        for (const userId of body?.users_id) {
            await database.UserMeetings.create( { meeting_id: meeting.id, user_id: userId, project_id: body.project_id });
        }

        const success: SuccessDTO = new SuccessDTO();
        success.status = 200;
        success.message = 'Meeting created and assigned to all users';
        success.data = null;

        response.status(200).json(success);
    } catch (error) {
        next(error);
    }
});

/**
 * Route for updating meeting, only admin permission
 * **/
MeetingsRouter.put('/:id', permission, async (request: Request, response: Response, next: NextFunction) => {
    try {
        const id = Number(request.params.id);
        if(isNaN(id)) throw new InvalidDataError({ message: 'Invalid meeting ID!', status: 422 });

        const body: MeetingUpdateDto = request.body;

        const meeting = await database.Meeting.findOne({ where: { id: id } });
        if(meeting === null) throw new InvalidDataError({ message: 'Invalid meeting ID!', status: 422 });

        const usersMeetingsId = (await database.UserMeetings.findAll({ attribute: ['user_id'], where: { 'meeting_id': meeting.id } }))
                                                     .map((userMeeting: any) => userMeeting.user_id);

        const usersMeetings = new Set<number>(usersMeetingsId);
        const usersMeetingsUpdate = new Set<number>(body.users_id);

        const deleteUserMeetings: number[] = [];
        const createUserMeetings: number[] = [];

        usersMeetings.forEach((value: number) => {
            if(!usersMeetingsUpdate.has(value)) deleteUserMeetings.push(value);
        });

        usersMeetingsUpdate.forEach((value: number) => {
            if(!usersMeetings.has(value)) createUserMeetings.push(value);
        });

        // delete excluded users
        for (const id of deleteUserMeetings) {
            await database.UserMeetings.destroy({ where: { 'user_id': id } });
        }

        // create updated user meetings
        for (const id of createUserMeetings) {
            await database.UserMeetings.create({ 'user_id': id, 'meeting_id': meeting.id, status: STATUSES.ACTIVE });
        }

        const success: SuccessDTO = new SuccessDTO();
        success.status = 200;
        success.message = 'Meeting updated successfully';
        // success.data = await meeting.save();

        response.status(success.status).json(success);
    } catch (error) {
        next(error);
    }
});

MeetingsRouter.delete('/:id', permission, async (request: Request, response: Response, next: NextFunction) => {
    const id = Number(request.params.id);

    try {
        if(isNaN(id)) throw new InvalidDataError({ message: 'Invalid meeting ID!', status: 422 });

        const meeting = await database.Meeting.findOne({ where: { 'id': id }});
        const userMeetings = await database.UserMeetings.findAll({ where: { 'meeting_id': id }})

        await meeting.destroy();

        for (const userMeeting of userMeetings) {
            await userMeeting.destroy();
        }

        response.status(200).json({ message: 'success' });
    } catch (error) {
       next(error);
    }
});

MeetingsRouter.get('/filters', permission, async (request: Request, response: Response, next: NextFunction) => {
    try {
        const users = await database.User.findAll({ attributes: [ 'id', 'name', 'username' ] });
        const projects = await database.Project.findAll({ attributes: [ 'id', 'name', ] });

        const success: SuccessDTO = new SuccessDTO();
        success.status = 200;
        success.message = 'Data needed for filtering(users and projects)';

        success.data = { users, projects };
        response.status(success.status).json(success);
    } catch (error) {
        next(error);
    }
});

export { MeetingsRouter as MeetingRoutes };
