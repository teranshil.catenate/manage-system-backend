import * as express from "express";
import {NextFunction, Request, Response} from "express";
import database from "../models";
import {authorization, permission} from "../middlewares/authentication.middleware";
import {SuccessDTO} from "../dto/response/success.dto";
import {TaskCreateDTO} from "../dto/request/task/task.create.dto";
import {InvalidDataError} from "../errors/InvalidDataError";
import {pagination} from "../middlewares/pagination.middleware";
import {Op} from "sequelize";

const TasksRouter = express.Router();

TasksRouter.use(authorization);

TasksRouter.get('/', permission, pagination, async (request: Request, response: Response, next: NextFunction) => {
    const { offset, limit, order } = request.body.pagination;

    try {
        const result = await database.Task.findAndCountAll({
            include: [ { model: database.User, as: 'user', attributes: ['id', 'username'] }],
            order, offset, limit
        });

        const success = new SuccessDTO();
        success.message = 'success';
        success.status = 200;
        success.data = result.rows;
        success.pagination = {
            total: result.count,
            offset: offset,
            count: limit
        };

        response.status(200).json(success);
    } catch (error) {
        next(error);
    }


});

TasksRouter.post('/', permission, async (request: Request, response: Response, next: NextFunction) => {

    try {
        const data: TaskCreateDTO = request.body.data;
        if( data.user_id && isNaN(Number(data.user_id)) ) throw new InvalidDataError({ message: 'Invalid user id type or invalid type!', status: 422});
        if( data.project_id && isNaN(Number(data.project_id)) ) throw new InvalidDataError({ message: 'Invalid project id type or invalid type!', status: 422});

        const userProjectAssignment = await database.UserProjects.findOne({ attributes: ['id'],
            where: { [Op.and]: [ { 'user_id': data?.user_id }, { 'project_id': data?.project_id } ] }});

        if(userProjectAssignment === null) throw new InvalidDataError({ message: 'User is not assign to this project', status: 422 });

        const task = await database.Task.create(data);

        const success = new SuccessDTO();
        success.message = 'Task created successfully!';
        success.status = 200;
        success.data = task;

        response.status(success.status).json(success);
    } catch (error) {
        next(error);
    }
});

TasksRouter.put('/', permission, async (request: Request, response: Response, next: NextFunction) => {
    try {
        const { id, ...data } = request.body.data;
        const task = await database.Task.findOne({ attributes: ['id'], where: { id }});
        if(task === null) throw new InvalidDataError({ message: 'ID is incorrect or invalid type', status: 422 });

        const userProjectAssignment = await database.UserProjects.findOne({ attributes: ['id'],
            where: { [Op.and]: [ { 'user_id': data?.user_id }, { 'project_id': data?.project_id } ] }});

        if(userProjectAssignment === null) throw new InvalidDataError({ message: 'User is not assign to this project', status: 422 });

        task.set({ ...data });
        const updateTask = await task.save();

        const success = new SuccessDTO();
        success.message = 'Task updated successfully!';
        success.status = 200;
        success.data = updateTask;

        response.status(success.status).json(success);
    } catch (error) {
        next(error);
    }
});

TasksRouter.delete('/:id', permission, async (request: Request, response: Response, next: NextFunction) => {
    try {
        const taskId = request.params.id;
        if(isNaN(Number(taskId))) throw new InvalidDataError({ message: 'Invalid task id type', status: 422 });

        const task = await database.Task.findOne({ attributes: ['id'], where: { id: taskId }});
        if(task === null) throw new InvalidDataError({ message: 'Task which id does not exist', status: 422 });

        await database.Task.destroy({ where: { id: taskId } });

        const success: SuccessDTO = new SuccessDTO();
        success.status = 200;
        success.message = 'Task deleted!';
        success.data = null;

        response.status(success.status).json(success);
    } catch (error) {
        next(error);
    }
});


export { TasksRouter as TaskRoutes };