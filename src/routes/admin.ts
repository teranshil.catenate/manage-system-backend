import * as express from "express";
import {NextFunction, Request, Response} from "express";
import {authorization, permission} from "../middlewares/authentication.middleware";
import {UserUpdateDto} from "../dto/request/user-update.dto";
import database from "../models";
import {SuccessDTO} from "../dto/response/success.dto";
import {Error, Model, ValidationError} from "sequelize";
import {ProjectCreateDto} from "../dto/request/project-create.dto";
import {pagination} from "../middlewares/pagination.middleware";
import {upload} from "../middlewares/upload.public.middleware";

const AdminRouter = express.Router();

AdminRouter.use(authorization);
AdminRouter.use(permission);

AdminRouter.put('/user', upload.single('picture'), async (request: Request, response: Response, next: NextFunction) => {
    try {
        console.log('heere');
        const body: UserUpdateDto = request.body;
        const user = await database.User.findOne({ where: { id: body.id } });

        if(body.username && body.username !== user.username) {
            const isUsernameUnique = await database.User.findOne({ where: { username: body.username } });
            if(isUsernameUnique !== null) throw new Error('Username is not unique');
        }

        if (user === null) throw new Error('Invalid user id');

        user.set({ ...body, picture: request.file?.filename || user.picture });
        await user.save();

        const success = new SuccessDTO();
        success.status = 200;
        success.message = 'User updated successfully';

        response.status(200).json(success);
    } catch (error) {
        next(error);
    }
});

AdminRouter.get('/projects', pagination, async (request: Request, response: Response, next: NextFunction) => {
    const userId: number | null = Number(request.query?.user);
    const { offset, limit, order } = request.body.pagination;
    let queryParams = {};

    if(!Number.isNaN(userId)) queryParams = { ...queryParams, include: { model: database.User, where: { id: userId }, as: 'users' }};

    database.Project.findAndCountAll({ ...queryParams, offset, limit, order })
        .then((result: { count: number[], rows: Model[] }) => {
            const success = new SuccessDTO();

            success.message = 'Successfully retrieved projects';
            success.status = 200;
            success.data = result.rows;
            success.pagination = {
                total: result.count,
                offset: offset,
                count: limit
            };

            response.status(200).json(success);
        })
        .catch((error: any) => {
            next(error);
        })
});

AdminRouter.post('/projects',async (request: Request, response: Response, next: NextFunction) => {
    const body: ProjectCreateDto = request.body;

    try {
        if((!body.finishDate && body.startDate) || (body.finishDate && !body.startDate)) {
            throw new ValidationError('Both start and finish date should be supplied');
        }
        await database.Project.create({ ...body })
            .then((project: any) => {
                const success: SuccessDTO = new SuccessDTO();
                success.status = 200;
                success.message = 'Project created successfully';
                success.data = project;

                response.status(success.status).json(success);
            })
            .catch((error: ValidationError) => { throw error });

    } catch (error) {
       next(error);
    }
});

AdminRouter.put('/projects', async (request: Request, response: Response, next: NextFunction) => {
    const body: ProjectCreateDto = request.body;

    try {
        const project = await database.Project.findOne({ where: { id: body.id } });
        const { id, ...updateData } = body;

        project.set({ ...updateData });

        await project.save();
        response.status(200).json({ message: 'success'});

    } catch (error) {
        next(error);
    }
});

export { AdminRouter as AdminRoutes };