import * as express from 'express';
import {Response, Request, NextFunction} from 'express';
import database from "../models/index";
import { ValidationErrorItem } from 'sequelize/dist';
import { UserRegisterDto } from "../dto/request/user-register.dto";
import { PasswordHash } from "../security/passwordHash";
import { AuthenticationDTO } from "../dto/response/authentication.dto";
import { UserDTO } from "../dto/response/user.dto";
import { JWT } from "../security/jwt";
import { upload } from "../middlewares/upload.public.middleware";
import { LoginDTO } from "../dto/request/login.dto";
import {SuccessDTO} from "../dto/response/success.dto";
import {InvalidDataError} from "../errors/InvalidDataError";
import {ValidationError} from "sequelize";

const AuthRouter = express.Router();

/**
 *
 * **/
AuthRouter.post('/register', upload.single('picture'), async (request: Request, response: Response, next: NextFunction) => {
    try {
        const body: UserRegisterDto = request.body;
        if(body.password && body.password.length < 8 && body.password.length > 20) {
            throw new Error('Password is not specified!');
        }

        if(body.password !== body.repeatPassword) throw new Error("Password and repeat password don't match!");

        body.password = await PasswordHash.hashPassword(body.password);
        body.picture = request.file?.filename || 'default.png'

        database.User.create({ ...body })
            .then( async (user: any) => {
                const authenticationDTO: AuthenticationDTO = new AuthenticationDTO();
                const success = new SuccessDTO();

                authenticationDTO.user = new UserDTO(user);
                authenticationDTO.token = await JWT.generateToken(user);

                success.status = 200;
                success.message = 'User successfully registered!';
                success.data = authenticationDTO;

                response.status(200).json( success );
            })
            .catch((error: ValidationError)  => {
                const message = error.errors.reduce((acc: string[], error: ValidationErrorItem) => [...acc, error.message ], []).join('\n');
                next( new InvalidDataError({ message, status: 422 }));
            });
    } catch (error) {
        next(error);
    }
});

AuthRouter.post('/login', async (request: Request, response: Response, next: NextFunction) => {
    const body: LoginDTO = request.body;
    const user = await database.User.findOne({ where: { username: body.username } });

    try {
        if(user === null) throw new InvalidDataError({ message: 'Supplied credentials are incorrect!', status: 422});
        if(body.password && !(await PasswordHash.isPasswordValid(body.password, user.dataValues.password))) {
            throw new InvalidDataError({ message: 'Supplied credentials are incorrect!', status: 422});
        }

        const authenticationDTO: AuthenticationDTO = new AuthenticationDTO();

        authenticationDTO.user = new UserDTO(user);
        authenticationDTO.token = await JWT.generateToken(user);

        response.json( { ...authenticationDTO });
    } catch (error) {
        next(error);
    }
});

export { AuthRouter as AuthRoutes };