import * as express from "express";
import {pagination} from "../middlewares/pagination.middleware";
import {authorization} from "../middlewares/authentication.middleware";
import {NextFunction, Request, Response} from "express";
import database from "../models";
import {Model} from "sequelize";
import {SuccessDTO} from "../dto/response/success.dto";
import {STATUSES} from "../constants";
import {PasswordHash} from "../security/passwordHash";
import {InvalidDataError} from "../errors/InvalidDataError";
import {log} from "util";

const NotificationsRouter = express.Router();

NotificationsRouter.get('/', authorization, pagination, (request: Request, response: Response, next: NextFunction) => {
    const { offset, limit, order } = request.body.pagination;

    database.Notification.findAndCountAll({
        where: { 'user_id': request.body.authenticated.id },
        include: { model: database.User, as: 'user' },
        attributes: [ 'id', 'description', 'seen', 'status', 'updated_at' ], order, offset, limit })
        .then((result: { count: number[], rows: Model[] }) => {
            const success = new SuccessDTO();
            success.message = 'success';
            success.status = 200;
            success.data = result.rows;
            success.pagination = {
                total: result.count,
                offset: offset,
                count: limit
            };

            response.status(200).json(success);
        })
        .catch((error: Error) => next(error));
});

NotificationsRouter.put('/:id', authorization, async (request: Request, response: Response, next: NextFunction) => {
    try {
        const notificationId = request.params.id;
        const userId = request.body.authenticated.id;

        console.log(notificationId, userId);
        if(isNaN(Number(notificationId))) throw new InvalidDataError({ message: 'Invalid type of notification ID!', status: 422 });

        const notification = await database.Notification.findOne({ where: { id: notificationId, user_id: userId } });
        if(notification === null) throw new InvalidDataError({ message: 'Invalid notification ID', status: 422 });

        notification.set({ seen: STATUSES.INACTIVE });
        await notification.save();

        const success: SuccessDTO = new SuccessDTO();
        success.status = 200;
        success.message = 'Notification marked as seen';
        success.data = null;

        response.status(success.status).json(success);
    } catch (error) {
        next(error);
    }
});

export { NotificationsRouter as NotificationRoutes };