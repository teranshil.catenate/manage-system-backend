import * as express from "express";
import {NextFunction, Request, Response} from "express";
import database from "../models";
import {JWT} from "../security/jwt";
import {Model} from "sequelize";
import {authorization, permission} from "../middlewares/authentication.middleware";
import {SuccessDTO} from "../dto/response/success.dto";
import {InvalidDataError} from "../errors/InvalidDataError";

const ProjectRouter = express.Router();

/**
 * Returns details information for projects(its fields and all users assigned to it)
 * Check if user assigned to project, therefore have access to it
 * */
ProjectRouter.get('/:id', authorization, async (request: Request, response: Response, next: NextFunction) => {

    const decode = JWT.decode(request.body.token);
    const projectId: number = parseInt(request.params.id);

    try {
        // TODO think if it is possible to be done with one query
        const userProject: Model | null = await database.Project.findOne( { attributes: [ 'id' ],
            where: { 'id': projectId },
            include: [ {
                model: database.User,
                as: 'users', attributes: [ 'id' ],
                where: { 'id': decode.id },
                through: { attributes: [] }
            } ]});

        if(!userProject) throw new InvalidDataError({ message: 'User is not assigned to this project', status: 422 });

        const project =  await database.Project.findOne({
            where: { 'id': projectId },
            include: [ {
                model: database.User,
                as: 'users', attributes: [ 'id', 'picture', 'username', 'name' ],
                through: { attributes: [] }
            }]
        });

        const tasks = await database.Task.findAll({
            attributes: [ 'id', 'description', 'title', 'start_date', 'end_date' ],
            where: { 'project_id': projectId, 'user_id': decode.id },
        });
        project?.setDataValue('tasks', tasks);

        const success: SuccessDTO = new SuccessDTO();
        success.status = 200;
        success.message = 'success';
        success.data = project;;
        response.status(200).json( success );

    } catch (error) {
        next(error);
    }
});

ProjectRouter.delete('/:id', authorization, permission, async (request: Request, response: Response, next: NextFunction) => {
    const id = request.params.id;
    try {
        if(isNaN(+id)) throw new InvalidDataError({ message: 'Incorrect type for project ID', status: 422 });

        await database.Project.destroy({ where: { id } });
        const meetings = await database.Meeting.findAll({ attributes: [ 'id' ], where: { 'project_id': id } });
        const destroyedMeetingsId = meetings.map((meeting: any) => meeting.id );

        await database.UserMeetings.destroy({ where: { 'meeting_id': [...destroyedMeetingsId] }});
        await database.Meeting.destroy({ where: { 'project_id': id } })
        await database.UserProjects.destroy({ where: { 'project_id': id } });

        const success: SuccessDTO = new SuccessDTO();
        success.status = 200;
        success.message = 'Project and its associations removed!';
        success.data = null;

        response.status(success.status).json(success);
    } catch (error) {
        next(error);
    }

});


export { ProjectRouter as ProjectRoutes };