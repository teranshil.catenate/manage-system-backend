import {Model} from 'sequelize';
import {STATUSES} from "../constants";

interface UserMeetingsAttributes {
    id: number;
    user_id: number;
    meeting_id: number;
    status: number;
}

module.exports = (sequelize: any, DataTypes: any) => {
    class UserProjects extends Model<UserMeetingsAttributes> implements UserMeetingsAttributes{

        id: number;
        meeting_id: number
        user_id: number;
        status: number;

        static associate(models: any) {}
    }

    UserProjects.init({

        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'users',
                key: 'id'
            }
        },
        meeting_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'meetings',
                key: 'id'
            }
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: STATUSES.ACTIVE
        }
    }, {
        sequelize,
        modelName: 'UserMeetings',
        tableName: 'user_meetings',
        timestamps: false
    });

    return UserProjects;
}

