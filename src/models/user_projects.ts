import {Model} from 'sequelize';

interface UserProjectsAttributes {
    id: number;
    user_id: number;
    project_id: number;
}

module.exports = (sequelize: any, DataTypes: any) => {
    class UserProjects extends Model<UserProjectsAttributes> implements UserProjectsAttributes{

        id: number;
        project_id: number;
        user_id: number;

        static associate(models: any) {}
    }

    UserProjects.init({

        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        user_id: {
            type: DataTypes.INTEGER,
            references: {
                model: 'users',
                key: 'id'
            }
        },
        project_id: {
            type: DataTypes.INTEGER,
            references: {
                model: 'projects',
                key: 'id'
            }
        }
    }, {
        sequelize,
        modelName: 'UserProjects',
        tableName: 'user_projects',
        timestamps: false
    });

    return UserProjects;
}

