import {Model} from 'sequelize';
import {ROLES, STATUSES} from "../constants";

export interface UserAttributes {
    id: number;
    username: string;
    name: string;
    lastName: string;
    picture: string;
    password: string;
    role: number;
    status: number;
}

module.exports = (sequelize: any, DataTypes: any) => {
    class User extends Model<UserAttributes> implements UserAttributes {

        id!: number;
        lastName!: string;
        name!: string;
        username!: string;
        password!: string;
        picture!: string;
        role!: number;
        status: number;

        static associate(models: any) {
            User.belongsToMany(models.Meeting, { through: models.UserMeetings, foreignKey: 'user_id' });
            User.belongsToMany(models.Project, { through: models.UserProjects, foreignKey: 'user_id', as: 'users' });
            User.hasOne(models.Notification);
            User.hasOne(models.Task);
        }
    }

    User.init({

        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },

        username: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },

        name: {
            type: DataTypes.STRING,
            allowNull: false
        },

        lastName: {
            type: DataTypes.STRING,
            field: 'last_name',
            allowNull: false
        },

        picture: {
            type: DataTypes.STRING,
            defaultValue: 'default.png'
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false,
        },

        role: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isIn: {
                    args: [[ ROLES.USER, ROLES.ADMIN ]],
                    msg: 'Invalid value for user role supplied!'
                }
            }
        },

        status: {
            type: DataTypes.SMALLINT,
            defaultValue: STATUSES.ACTIVE
        }

    }, {
        sequelize,
        modelName: 'User',
        tableName: 'users',
        timestamps: false,
        underscored: true
    });

    return User;
}

