import {Model} from 'sequelize';
import {ROLES, STATUSES} from "../constants";

interface TaskAttributes {
    id: number;
    description: string;
    title: string;
    user_id: number;
    project_id: number;
    start_date: string;
    end_date: string;
    status: number;
}

module.exports = (sequelize: any, DataTypes: any) => {
    class Task extends Model<TaskAttributes> implements TaskAttributes {

        id: number;
        description: string;
        title: string;
        user_id: number;
        project_id: number;
        start_date: string;
        end_date: string;
        status: number;

        static associate(models: any) {
            Task.belongsTo(models.User, { foreignKey: 'user_id', as: 'user' });
            Task.belongsTo(models.Project, { foreignKey: 'project_id', as: 'project' });
        }
    }

    Task.init({
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        description: {
          type: DataTypes.TEXT
        },
        title: {
            type: DataTypes.STRING
        },
        start_date: {
            type: DataTypes.DATE,
            allowNull: false
        },
        end_date: {
            type: DataTypes.DATE
        },
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'users',
                key: 'id'
            }
        },
        project_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'projects',
                key: 'id'
            }
        },
        status: {
            type: DataTypes.SMALLINT,
            defaultValue: STATUSES.ACTIVE
        }
    }, {
        sequelize,
        modelName: 'Task',
        tableName: 'tasks',
        timestamps: false,
        underscored: true
    });

    return Task;
}

