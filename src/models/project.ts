import {Model} from 'sequelize';
import {STATUSES} from "../constants";

interface ProjectAttributes {
    id: number;
    name: string;
    budget: number;
    description: string;
    start_date: Date;
    finish_date: Date;
    status: number;
}

module.exports = (sequelize: any, DataTypes: any) => {
    class Project extends Model<ProjectAttributes> implements ProjectAttributes {

        id!: number;
        name!: string;
        budget: number;
        description: string;
        start_date: Date;
        finish_date: Date;
        status: number;

        static associate(models: any) {
            Project.hasMany(models.Meeting);
            Project.belongsToMany(models.User, { through: models.UserProjects, foreignKey: 'project_id', as: 'users' });
            Project.hasOne(models.Task, { as: 'tasks' });
        }
    }

    Project.init({

        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },

        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        },
        budget: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        description: {
          type: DataTypes.TEXT
        },
        finish_date: {
          type: DataTypes.DATE,
        },
        start_date: {
          type: DataTypes.DATE
        },

        status: {
            type: DataTypes.INTEGER,
            defaultValue: STATUSES.ACTIVE
        }

    }, {
        sequelize,
        modelName: 'Project',
        tableName: 'projects',
        timestamps: false,
        underscored: true
    });

    return Project;
}

