import {Model} from 'sequelize';

export interface MeetingAttributes {
    id: number;
    name: string;
    description: string;
    start: string;
    end: string;
}

module.exports = (sequelize: any, DataTypes: any) => {
    class Meeting extends Model<MeetingAttributes> implements MeetingAttributes{

        id!: number;
        name!: string;
        description!: string;
        start!: string;
        end!: string;

        static associate(models: any) {
            Meeting.belongsTo(models.Project, { foreignKey: 'project_id', as: 'project' });
            Meeting.belongsToMany(models.User, { through: models.UserMeetings, foreignKey: 'meeting_id', as: 'users' });
        }
    }

    Meeting.init({

        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },

        name: {
            type: DataTypes.STRING,
            allowNull: false
        },

        description: {
            type: DataTypes.STRING,
            allowNull: false
        },

        start: {
            type: DataTypes.DATE,
            allowNull: false
        },

        end: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: 'Meeting',
        tableName: 'meetings',
        timestamps: false,
        underscored: true
    });

    return Meeting;
}

