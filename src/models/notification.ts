import {Model} from 'sequelize';
import {STATUSES} from "../constants";

interface NotificationAttributes {
    id: number;
    seen: boolean;
    description: string;
    user_id: number;
    status: boolean;
}

module.exports = (sequelize: any, DataTypes: any) => {
    class Notification extends Model<NotificationAttributes> implements NotificationAttributes{

        id!: number;
        seen!: boolean;
        description: string;
        status: boolean;
        user_id: number;

        static associate(models: any) {
            Notification.belongsTo(models.User, { foreignKey: 'user_id', as: 'user' });
        }
    }

    Notification.init({

        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        seen: {
            type: DataTypes.INTEGER,
            defaultValue: STATUSES.ACTIVE
        },
        description: {
            type: DataTypes.TEXT
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: STATUSES.ACTIVE
        },
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'users',
                key: 'id'
            }
        },
    }, {
        sequelize,
        modelName: 'Notification',
        tableName: 'notifications',
        underscored: true
    });

    return Notification;
}

