export class ErrorDTO {
    status: number;
    message: string;
    data?: Object | [] | null;
}