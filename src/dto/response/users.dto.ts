export class UsersResponseDTO {
    id: number;
    username: string;
    name: string;
    lastName: string;
    picture: string;
    role: number;
}