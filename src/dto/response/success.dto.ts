export class SuccessDTO {
    status: number;
    message: string;
    data: {} | null;
    pagination: {} | null;
}