export class SuccessResponseDTO {
    status: number;
    message: string;
    data: {} | null;
}