export class UserDTO {
    id: number;
    username: string;
    name: string;
    lastName: string;
    picture: string;
    role: number;

    constructor(user: any) {
        this.id = user.id;
        this.username = user.username;
        this.name = user.name;
        this.lastName = user.lastName;
        this.picture = user.picture;
        this.role = user.role;
    }
}