import {ROLES, STATUSES} from '../../constants'

export class UserUpdateDto {
    id: number;
    oldPassword?: string;
    newPassword?: string;
    role?: ROLES;
    username?: string;
    name?: string;
    status?: STATUSES;
    repeatPassword?: string;
}