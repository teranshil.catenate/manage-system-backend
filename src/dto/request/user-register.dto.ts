import {ROLES, STATUSES} from "../../constants";

export interface UserRegisterDto {
    username: string;
    name: string;
    lastName: string;
    password: string;
    repeatPassword: string;
    role: ROLES;
    status?: STATUSES;
    picture?: string;
}