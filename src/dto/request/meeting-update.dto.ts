export class MeetingUpdateDto {
    id: number;
    name: string;
    description: string;
    start: string;
    end: string;
    users_id: number[];
}