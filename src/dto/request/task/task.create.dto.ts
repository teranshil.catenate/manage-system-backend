export class TaskCreateDTO {
    title: string;
    user_id: number;
    project_id: number;
    start_date: Date | string;
    end_date?: Date | string;
    description?: string;
    status: number
}