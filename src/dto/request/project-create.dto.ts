import {STATUSES} from "../../constants";

export class ProjectCreateDto {
    id: number;
    name: string;
    budget?: number;
    description?: string;
    startDate?: Date;
    finishDate?: Date;
    status: STATUSES;
}