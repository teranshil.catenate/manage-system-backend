export class MeetingCreateDto {
    id: number;
    name: string;
    description: string;
    start: string;
    end: string
    project_id: number;
    users_id: number[];
}