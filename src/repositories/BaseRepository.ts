import {Model} from "sequelize";
import database from "../models";

export abstract class BaseRepository {
    protected model: string;
    protected constructor(model: string) {
        this.model = model;
    }

    public async deleteRecordById(ids: number[]): Promise<Model<any>> {
        return await database[this.model].destroy({ where: { id: ids }});
    }

    public async getRecordById(id: number): Promise<Model<any> | null> {
        return await database[this.model].findOne({ where: { id }});
    }

    public async deleteRecordsByCondition(where: Record<string, any>) {
        return await database[this.model].destroy({ where });
    }

    public async getRecordsByCondition(where: Record<string, any>, fields: string[] = ['id']) {
        return await database[this.model].findAll({ attributes: fields, where });
    }

    public async createRecord(values: Record<string, any>) {
        return await database[this.model].create(values);
    }

}