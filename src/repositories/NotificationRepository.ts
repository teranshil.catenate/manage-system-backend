import {BaseRepository} from "./BaseRepository";

export class NotificationRepository extends BaseRepository {

    constructor() {
        super('Notification');
    }

}