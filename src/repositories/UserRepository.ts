import database from "../models";
import {PaginationType} from "../types/pagination-type";
import {BaseRepository} from "./BaseRepository";

export class UserRepository extends BaseRepository {
    constructor() {
        super('User');
    }

    public getUsersWithPagination(pagination: PaginationType, fields: string[] = ['id']) {
       const { offset, limit, order } = pagination;
       return database.User.findAndCountAll({ attributes: [ ...fields ], order, offset, limit })
    }

    public getUsers(fields: string[] = ['id']) {
        return database.User.findAndCountAll({ attributes: [ ...fields ] });
    }

}