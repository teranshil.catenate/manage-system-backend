import database from "../models";
import {Model} from "sequelize";
import {MeetingAttributes} from "../models/meeting";
import {BaseRepository} from "./BaseRepository";

export class MeetingRepository extends BaseRepository {

    constructor() {
        super('Meeting');
    }

    public async getMeetingsByCondition(where: Record<string, any>, fields: string[] = ['id']) {
        return await database.Meeting.findAll({ attributes: fields, where: where });
    }

    public async getMeetingsDetailed(options: Record<string, any>): Promise<Model[]> {
        return await database[this.model].findAll(options);
    }
}