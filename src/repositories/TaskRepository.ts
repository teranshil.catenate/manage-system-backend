import {BaseRepository} from "./BaseRepository";

export class TaskRepository extends BaseRepository {

    constructor() {
        super('Task');
    }
}