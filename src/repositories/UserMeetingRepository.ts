import {BaseRepository} from "./BaseRepository";
import database from "../models";
import {Model} from "sequelize";

export class UserMeetingRepository extends BaseRepository {

    constructor() {
        super('UserMeetings');
    }
}