import {BaseRepository} from "./BaseRepository";

export class UserProjectRepository extends BaseRepository{

    constructor() {
        super('UserProjects');
    }
}