import {NextFunction, Request, Response} from "express";
import {UserRepository} from "../repositories/UserRepository";
import {UserUpdateDto} from "../dto/request/user-update.dto";
import {PasswordHash} from "../security/passwordHash";
import {InvalidDataError} from "../errors/InvalidDataError";
import {UserAttributes} from "../models/user";
import {Model, Op} from "sequelize";
import {UserMeetingRepository} from "../repositories/UserMeetingRepository";
import {UserProjectRepository} from "../repositories/UserProjectRepository";
import {TaskRepository} from "../repositories/TaskRepository";
import database from "../models";
import {STATUSES} from "../constants";
import {NotificationRepository} from "../repositories/NotificationRepository";
import {MeetingRepository} from "../repositories/MeetingRepository";

export default class UserService {
    private userRepository: UserRepository;
    private notificationRepository: NotificationRepository;

    constructor() {
        this.userRepository = new UserRepository();
        this.notificationRepository = new NotificationRepository();
    }

    public async updateUser(request: Request, response: Response, next: NextFunction): Promise<Model<UserAttributes>> {
        const body: UserUpdateDto = request.body;
        const user: Model<UserAttributes> | null = await this.userRepository.getRecordById(body.id);

        if (user === null) throw new InvalidDataError({ message: 'Invalid user', status: 422 });

        if (body.oldPassword && !(await PasswordHash.isPasswordValid(body.oldPassword, user.getDataValue('password')))) {
            throw new InvalidDataError({ message: 'Supplied old password is incorrect!', status: 422 });
        }

        if (body.newPassword && body.newPassword !== body.repeatPassword) {
            throw new InvalidDataError({ message: "New password doesn't match with repeat!", status: 422 });
        }

        user.set({
            password: body.newPassword ? await PasswordHash.hashPassword(body.newPassword) : user.getDataValue('password'),
            picture: request.file?.filename || user.getDataValue('picture')
        });

        return await user.save();
    }

    public getUsers(request: Request, response: Response, next: NextFunction) {
        const requiredFields = [ 'id', 'username', 'status', 'name', 'lastName', 'role', 'picture' ];

        if(request.body.pagination) {
            return this.userRepository.getUsersWithPagination(request.body.pagination, requiredFields);
        }

        return this.userRepository.getUsers(requiredFields);
    }

    /**
     * Delete user and its associations as tasks, project assignments.
     * Remove user from meeting if it's included.
     * */
    public async deleteUserById(request: Request) {
        if(isNaN(Number(request.params.id))) throw new InvalidDataError({ message: 'Invalid used ID!', status: 422 });
        const id = Number(request.params.id);

        const userMeetingRepository: UserMeetingRepository = new UserMeetingRepository();
        const userProjectRepository: UserProjectRepository = new UserProjectRepository();
        const userTaskRepository: TaskRepository = new TaskRepository();

        await userTaskRepository.deleteRecordsByCondition({ 'user_id': id });
        await this.userRepository.deleteRecordById([ id ]);

        await userMeetingRepository.deleteRecordsByCondition({ 'user_id': id });
        await userProjectRepository.deleteRecordsByCondition({ 'user_id': id });
    }

    public async assignUserToProject(request: Request): Promise<void> {
        const userId: string = request.params.userId;
        const projectId: string = request.params.projectId;

        if(isNaN(Number(userId))) throw new InvalidDataError({ message: 'User id is in invalid type!', status: 422 });
        if(isNaN(Number(projectId))) throw new InvalidDataError({ message: 'Project id is in invalid type!', status: 422 });

        const userProjectRepository: UserProjectRepository = new UserProjectRepository();

        const user = await this.userRepository.getRecordsByCondition({ id: userId });
        if(user === null) throw new InvalidDataError({ message: "User with that ID doesn't exists! " , status: 422 });

        const project = await userProjectRepository.getRecordsByCondition({ id: projectId }, ['id']);
        if(project === null) throw new InvalidDataError({ message: "Project with that ID doesn't exists! " , status: 422 });

        await userProjectRepository.createRecord({ 'project_id': projectId, 'user_id': userId, status: STATUSES.ACTIVE });
        await database.Notification.create({ description: `You have been added to project: ${project.name}`, 'user_id': userId });
    }

    public async removeUserFromProject(request: Request) {
        const userProjectRepository: UserProjectRepository = new UserProjectRepository();
        const userId: string = request.params.userId;
        const projectId: string = request.params.projectId;

        if(isNaN(Number(userId))) throw new InvalidDataError({ message: 'User id is in invalid type!', status: 422 });
        if(isNaN(Number(projectId))) throw new InvalidDataError({ message: 'Project id is in invalid type!', status: 422 });

        await userProjectRepository.deleteRecordsByCondition({ [Op.and]: [ { 'project_id': projectId }, { 'user_id': userId } ] });
    }

    public async getUserMeetingsDetailed(request: Request) {
        const userId = request.body.authenticated.id;

        if(isNaN(Number(userId))) throw new InvalidDataError({ message: 'User id is in invalid type!', status: 422 });
        const meetingRepository: MeetingRepository = new MeetingRepository();
        const queryOptions = {
            include: [
                { model: database.User, as: 'users', attributes: [], where: { id: userId }, through: { attributes: [] }},
                { model: database.Project, as: 'project', attributes: ['id', 'name'] }
            ]
        };

        const meetings = await meetingRepository.getMeetingsDetailed(queryOptions);
    }
}