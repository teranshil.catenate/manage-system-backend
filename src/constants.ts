export enum ROLES {
    USER = 0,
    ADMIN = 1
}

export enum STATUSES {
    ACTIVE = 0,
    INACTIVE = 1
}

export enum QUERY_ORDER {
    DESC = 1,
    ASC = -1,
}