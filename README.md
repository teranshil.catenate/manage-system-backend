# manage-system-backend

## Setup

- [ ] [download](https://www.postgresql.org/download) and install postgres
- [ ] make sure that your database credentials are correct inside .env file 
- [ ] ``npm install``
- [ ] start server -> ``npm run start``, running the command will create the datatables for you
- [ ] exported postman file with all supported endpoints -> **postman-endpoints.json**

### Used libraries documentations ###

- sequelize ORM - https://sequelize.org/master/manual/
- database postgres - https://www.postgresql.org/